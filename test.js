


const TEST = require('./testFile/index');

const { squareNum, objectRender, getReq, getString } = require('./functionsToTest');
const fs = require('fs');

const personData = {
    name: 'Josh',
    age: 27,
    hobbies: ['skiing', 'motorcycles'],
    address: {
        number: 86,
        street: 'Baptist Street',
        suburb: 'Redfern',
        state: 'NSW',
        postCode: 2016
    }
}

TEST({
    testName: 'getRequestTest',
    testFunction: getReq('https://jsonplaceholder.typicode.com/todos')
        .then(data => data[0]),
    assertions: ['objectHasProps'],
    expectedOutput: { userId: 1,
        id: 1,
        title: 'Lawrences book',
        completed: false }
});

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(100),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 10000
});

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(2),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 4
});

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(4),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 16
})

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(3),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 9
})

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(5),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 25
})

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(6),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 36
})

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(7),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 49
})

TEST({
    testName: 'squareNum(100)',
    testFunction: squareNum(8),
    assertions: ['valueEquals', 'typeEquals'],
    expectedOutput: 64
});


