
const EventEmitter = require('events');

// Expected input to TestRunner
    // if we have tests coming from activities p1c2, p13c4 and shared

/*  

const test1 = {
    name: 'test1',
    output: 100,
    func: funcToTest,
    args: [10],
    assertions: ['equals']
}

input to TestRunner Class
module.exports = [
        {
            dir: 'p1c2',
            tests: [
                test1,
                test2
            ]
        },
         {
            dir: 'p13c4',
            tests: [
                test3,
                test4
            ]
        },
         {
            dir: 'shared',
            tests: [
                test5,
                test6,
                test7
            ]
        }
    ]
*/

/**
 * EXAMPLE FUNCTION
 *  (testFunction, expectedOutput) => {
        it(`Input value should contain the properties: ${Object.keys(expectedOutput)}`, async () => {
            let inputData = await testFunction;
            await expect(Object.keys(inputData)).to.include.members(Object.keys(expectedOutput));
        });
 */

class Test {
    constructor(test, activity) {
        // test must be of type object
        const { name, output, func, args, assertions } = test;
        this.activity = activity;
        this.name = name; // must be string
        this.output = output; // expected output of function
        this.func = func; // must be function
        this.args = args; // either array or single value
        this.assertions = assertions; // must be array of functions, or a single function
    }

    runTest() {

    }
}

class Activity extends EventEmitter {
    constructor(activity, testRunner) {
        const { dir, tests } = activity;

        this.testRunner = testRunner;
        this.dir = dir;
        this.tests = tests;
        this.amtTests = tests.length;
        this.testsComplete = 0;

        this.initTests();

        this.on('test_complete', this.nextTest);
    }

    initTests() {
        this.tests = this.tests.map(test => new Test(test, this))
    }

    nextTest() {
        this.testsComplete++;
        if (!this.testsComplete == this.amtTests) {
            this.tests[this.testsComplete].runTest();
        } else this.testRunner.emit('activity_complete', this);
    }
}

module.exports = class TestRunner extends EventEmitter {
    constructor(activities) {
        this.activities = activities; // must be array
        this.amtActivities = activities.length;

        this.activitiesComplete = 0;

        this.initActivities();

        this.on('activity_complete', this.nextActivity);
        
    }

    initActivities() {
        this.activities = this.activities.map(activity => new Activity(activity, this));
    }

    nextActivity() {
        this.activitiesComplete++;
        if (!this.activitiesComplete == this.amtActivities) {
            this.activities
        }
    }

    onComplete() {

    }
}