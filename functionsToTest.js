

const https = require('https');

const squareNum = number => number * number;

const objectRender = object => object;

const getReq = (url) => {
    return new Promise((resolve, reject) => {

        https.get(url, resp => {

            let data = '';
            resp.on('data', chunk => data += chunk);
            resp.on('end', () => resolve(data));
        });
    })
    .then(rawData => JSON.parse(rawData));
}

const getString = string => string;

module.exports = {
    squareNum,
    objectRender,
    getReq,
    getString
}