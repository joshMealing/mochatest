

# Testing framework

### Install mocha and download the testing package from the @accordant repo

### Include a file named 'test.js' in the root directory of your project
    In this file, you must require the testing package and a bunch of exported functions to test

```javascript
const TEST = require('testPackage');
const { func1, func2, func3 } = require('pathToFunctionsFile');
```

### Now to initialise a test it is as simple as calling the __TEST__ function with some options.  All options are mandatory

```javascript
    TEST({
        testName: String,
        testFunction: exportedFunction(...args),
        assertions: String || [ String ],
        expectedOutput: 'Whatever the test function is expected to output'
    });
```
---

## Options

__testName__ : A string to be able to discerne one test from another

__testFunction__ : The value for this property should be an invoked function with all arguments passed in. If this value is a promise, it can be chained to get the required output. The function can return a string, array, object, number, or any value you could think of.

__assertions__ : A string or array of strings can be passed, with predefined keywords. Each of these assertions will perform a check for this __testFunction__. A list of assertions is provided down below with an explanation of what each achieves

__expectedOutput__ : The data which is expected to be output from the testFunction goes here.

---

# Example

## Here we are testing a function that capitalises the first word of a string passed in as an argument.

## Let's run a simple test

### Exported Functions

This function is somewhere in our experience code. We need to export it to make it accesible in our test.js file
```javascript
    const capitaliseWord = string => {
        let firstLetter = string.charAt(0);
        let restOfWord = string.slice(1, string.length);
        return firstLetter + restOfWord;
    }

    exports.capitaliseWord = capitaliseWord;

```

---

### test.js

```javascript 
    const TEST = require('testPackage');
    const { capitaliseWord } = require('pathToFunctionsFile');

    TEST({
        testName: 'capitaliseWord',
        testFunction: capitaliseWord('hello world'),
        assertions: ['valueEquals', 'typeEquals'],
        expectedOutput: 'Hello world'
    });
```

---

# We're done! Just run the command 
    npm run test      
# in your console

---

### This test will check that the value of the input equals the expected output, as well as making sure the type is the same.

Many tests can be declared the same way in the test.js file. You can have 1, 5 or even 100 tests in the same file.

---

# Assertions

There are a few assertions vailable at the moment, with more being added in the future as needed

* valueEquals: 
    * checks a single value such as a string or number. Not used with objects or arrays

* typeEquals: 
    * checks the type of input is the same as output. Used for all types

* objectHasProps: 
    * A check for JSON objects to ensure they contain the properties outlined in the expectedOutput. 
    * If there are more properties being returned from the function, that is fine, this assertion is to check that the returned value **contains** the things we want

* objectEquals:
    * Used for objects and arrays
    * Check that the Keys and the values for an object, or the indexes of an array are the same as the expectedOutput
    * If an array is being tested, then the order of the indices are checked. If the order doesn't match, the test will fail

* includes:
    * Can be used with all types except numbers
    * checks if a string contains a substring
    * checks objects have a particular key:value pair or set of key:value pairs
    * checks if an array contains one specific value.

* arrayContains:
    * Checks that an array contains a set of values. Order does not matter
    * if the input  function returns [1, 3, 4, 5], then passing in an expectedOutput of [5, 4, 1] will pass this test



