// Expected input to TestRunner
    // if we have tests coming from activities p1c2, p13c4 and shared

/*  

const test1 = {
    name: 'test1',
    output: 100,
    func: funcToTest,
    args: [10],
    assertions: ['equals']
}

input to TestRunner Class
module.exports = [
        {
            dir: 'p1c2',
            tests: [
                test1,
                test2
            ]
        },
         {
            dir: 'p13c4',
            tests: [
                test3,
                test4
            ]
        },
         {
            dir: 'shared',
            tests: [
                test5,
                test6,
                test7
            ]
        }
    ]
*/

module.exports = class TestRunner {
    constructor(tests) {
        this.test = tests;
    }

    initTests() {

    }

    onComplete() {

    }
}