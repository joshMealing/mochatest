

const assertionFunctions = require('./lib/assertions');


module.exports = ({
    testName,
    testFunction,
    assertions,
    expectedOutput
}) => describe(`${testName}`, async () => {

    if (typeof assertions === 'string') return await assertionFunctions[assertions](testFunction, expectedOutput)
    else return await assertions.forEach(async assertion => assertionFunctions[assertion](testFunction, expectedOutput));

});


