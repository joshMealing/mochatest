
const { expect, should, assert } = require('chai');

// assertion for object containing properties

module.exports = {
    'objectHasProps': (testFunction, expectedOutput) => {
        it(`Input value should contain the properties: ${Object.keys(expectedOutput)}`, async () => {
            let inputData = await testFunction;
            await expect(Object.keys(inputData)).to.include.members(Object.keys(expectedOutput));
        });
    },

    'objectEquals': (testFunction, expectedOutput) => {
         it(`Input object should equal ${expectedOutput}`, async () => {
            let inputData = await testFunction;
            await assert.deepEqual(inputData, expectedOutput);

        });
    },

    'arrayContains': (testFunction, expectedOutput) => {
        it(`Input array should contain ${expectedOutput}`, async () => {
           let inputData = await testFunction;
           // await expect(inputData).to.eql(expectedOutput);
           await expect(inputData).to.include.members(expectedOutput);

       });
    },

    'valueEquals': (testFunction, expectedOutput) => it(`Input value should be strictly equal to ${expectedOutput}`, async () => {
        let inputData = await testFunction;
        await assert.strictEqual(inputData, expectedOutput);
    }),

    'typeEquals': (testFunction, expectedOutput) => {
        if (Array.isArray(expectedOutput)) {
            it(`Input value should be an array`, async () => {
                let inputData = await testFunction;
                await assert.isArray(inputData);
            });

        } else {
            it(`Input value should be ${typeof expectedOutput}`, async () => {
                let inputData = await testFunction;
                await assert.typeOf(inputData, typeof expectedOutput);
            });

        }
    },
    
    'includes': (testFunction, expectedOutput) => it(`Input should include this: ${JSON.stringify(expectedOutput)}`, async () => {
        let inputData = await testFunction;
        await expect(inputData).to.deep.include(expectedOutput);
    }),
}